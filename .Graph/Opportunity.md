# Inspire

## Corporate Sponsor: Elements.tv
- https://news.ycombinator.com/item?id=18579754
- https://elements.tv/


# Discussion:
- [I hope that more people would help with bcachefs development. | Hacker News](https://news.ycombinator.com/item?id=19697496)

Quote: "As a personal experience, I have two ZFS based file servers being hammered by researchers pushing it in many weird IO patterns, bursts requests and so for about a year and half without ever hitting a serious error. I'm amazed by how rock solid the system is. One of them even has a Optane cache for NFS writes- just to give you an idea of feature richness."

# Conclusion
Best option is to Combine ZFS with Bcachefs! use Bcache for databases and ZFS for Data Integrity! 